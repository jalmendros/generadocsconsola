package es.aborda.components
{
	import mx.collections.ArrayCollection;
	
	import spark.collections.Sort;
	import spark.collections.SortField;
	
	import es.aborda.control.consola.DameCamposMessage;
	import es.aborda.control.consola.DameDispositivosMessage;
	import es.aborda.control.consola.DameEntidadesMessage;
	import es.aborda.control.consola.DameEspecificosMessage;
	import es.aborda.control.consola.DameFiltrosMessage;
	import es.aborda.control.consola.DamePerfilesMessage;
	import es.aborda.control.consola.DameTiposRegistrosMessage;
	import es.aborda.control.consola.DameUsuariosPerfilMessage;
	import es.aborda.domain.Aplicaciones;
	import es.aborda.domain.Campos;
	import es.aborda.domain.Dispositivos;
	import es.aborda.domain.Entidades;
	import es.aborda.domain.Especificos;
	import es.aborda.domain.Filtros;
	import es.aborda.domain.Perfiles;
	import es.aborda.domain.TiposRegistro;
	import es.aborda.domain.Usuarios;
	
	[Bindable]
	public class TreeItem
	{
		public static var APLICACION:String = "Aplicacion";
		public static var PERFIL:String = "Perfil";
		public static var ENTIDAD:String = "Entidad";
		public static var TIPOREGISTRO:String = "TipoRegistro";
		public static var CAMPO:String = "Campo";
		public static var FILTRO:String = "Filtro";
		public static var ESPECIFICO:String = "Especifico";
		public static var USUARIO:String = "Usuario";
		public static var DISPOSITIVO:String = "Dispositivo";
		
		public function TreeItem(object:Object, type:String, orgName:String, dispatcher:Function)
		{
			this.object = object;
			this.type = type;
			this.dispatcher = dispatcher;
			this.orgName = orgName;
			
			var sort:Sort = new Sort();
			sort.fields = [new SortField("type"), new SortField("label")];
			children.sort = sort;
			
			loadChildren();
//			if (!hasChildren)
//				children = null;
		}
		
		private function loadChildren():void
		{
			if (!dispatcher)
				return;
			
			switch(type)
			{
				case APLICACION:
					children.removeAll();
					dispatcher(new DamePerfilesMessage((object as Aplicaciones).id, damePerfilesHandler));
					break;
				
				case PERFIL:
					children.removeAll();
					dispatcher(new DameEntidadesMessage((object as Perfiles).id, dameEntidadesHandler));
					dispatcher(new DameEspecificosMessage((object as Perfiles).id, dameEspecificosHandler));
					dispatcher(new DameUsuariosPerfilMessage((object as Perfiles).id, dameUsuariosPerfilHandler));
					break;
				
				case ENTIDAD:
					children.removeAll();
					dispatcher(new DameTiposRegistrosMessage((object as Entidades).id, dameTiposRegistrosHandler));
					dispatcher(new DameFiltrosMessage((object as Entidades).id, dameFiltrosHandler));
					break;
				
				case TIPOREGISTRO:
					children.removeAll();
					dispatcher(new DameCamposMessage((object as TiposRegistro).id, dameCamposHandler));
					break;
				
				case USUARIO:
					children.removeAll();
					dispatcher(new DameDispositivosMessage((object as Usuarios).id, dameDispositivosHandler));
					break;
			}
		}
		
		private function damePerfilesHandler(arr:ArrayCollection):void
		{
			for each (var obj:Perfiles in arr)
			{
				var treeItem:TreeItem = new TreeItem(obj, TreeItem.PERFIL, orgName, dispatcher);
				
				children.addItem(treeItem);
			}
			
			children.refresh();
		}
		
		private function dameEntidadesHandler(arr:ArrayCollection):void
		{
			for each (var obj:Entidades in arr)
			{
				var treeItem:TreeItem = new TreeItem(obj, TreeItem.ENTIDAD, orgName, dispatcher);
				
				children.addItem(treeItem);
			}
			
			children.refresh();
		}
		
		private function dameTiposRegistrosHandler(arr:ArrayCollection):void
		{
			for each (var obj:TiposRegistro in arr)
			{
				var treeItem:TreeItem = new TreeItem(obj, TreeItem.TIPOREGISTRO, orgName, dispatcher);
				
				children.addItem(treeItem);
			}
			
			children.refresh();
		}
		
		private function dameFiltrosHandler(arr:ArrayCollection):void
		{
			for each (var obj:Filtros in arr)
			{
				var treeItem:TreeItem = new TreeItem(obj, TreeItem.FILTRO, orgName, dispatcher);
				
				children.addItem(treeItem);
			}
			
			children.refresh();
		}
		
		private function dameEspecificosHandler(arr:ArrayCollection):void
		{
			for each (var obj:Especificos in arr)
			{
				var treeItem:TreeItem = new TreeItem(obj, TreeItem.ESPECIFICO, orgName, dispatcher);
				
				children.addItem(treeItem);
			}
			
			children.refresh();
		}
		
		private function dameUsuariosPerfilHandler(arr:ArrayCollection):void
		{
			for each (var obj:Usuarios in arr)
			{
				var treeItem:TreeItem = new TreeItem(obj, TreeItem.USUARIO, orgName, dispatcher);
				
				children.addItem(treeItem);
			}
			
			children.refresh();
		}
		
		private function dameCamposHandler(arr:ArrayCollection):void
		{
			for each (var obj:Campos in arr)
			{
				var treeItem:TreeItem = new TreeItem(obj, TreeItem.CAMPO, orgName, dispatcher);
				
				children.addItem(treeItem);
			}
			
			children.refresh();
		}
		
		private function dameDispositivosHandler(arr:ArrayCollection):void
		{
			for each (var obj:Dispositivos in arr)
			{
				var treeItem:TreeItem = new TreeItem(obj, TreeItem.DISPOSITIVO, orgName, dispatcher);
				
				children.addItem(treeItem);
			}
			
			children.refresh();
		}
		
		private var dispatcher:Function;
		public var object:Object;
		public var type:String;
		public var orgName:String;
		public var children:ArrayCollection = new ArrayCollection();
		private var _typeColor:uint = 0x8888ff;

		public function get typeColor():uint
		{
			if (type == DISPOSITIVO && Dispositivos(object).instalado)
				return 0xff0000;
			if (type == DISPOSITIVO && !Dispositivos(object).instalado && Dispositivos(object).reinstalar)
				return 0x00ff00;
			return _typeColor;
		}
		public function set typeColor(value:uint):void
		{
			_typeColor = value;
		}
		
		private var _colorLetter:uint = 0x640000;
		private var koColor:uint = 0x640000;
		private var okColor:uint = 0x006400;

		public function get colorLetter():uint
		{
			if (!children || children.length == 0)
			{
				if (_typeColor == 0x88ff88)
					_colorLetter = okColor;
			}
			else
			{
				_colorLetter = okColor;
				
				for each (var treeItem:TreeItem in children) 
				{
					if (treeItem.colorLetter != okColor)
						_colorLetter = koColor;
				}
			}
			
			return _colorLetter;
		}
		public function set colorLetter(value:uint):void
		{
			_colorLetter = value;
		}
		
		public function get id():int
		{
			return object.id;
		}
		
		public function get label():String
		{
			switch(type)
			{
				case APLICACION:
					return (object as Aplicaciones).nombre;
					
				case PERFIL:
					return (object as Perfiles).nombre;
					
				case ENTIDAD:
					return (object as Entidades).nombre;
					
				case TIPOREGISTRO:
					return (object as TiposRegistro).recordtypeid;
					
				case CAMPO:
					return (object as Campos).name;
					
				case FILTRO:
					return (object as Filtros).filtro;
					
				case ESPECIFICO:
					return (object as Especificos).operador + " - " + (object as Especificos).entidad + " - " + (object as Especificos).especifico;
					
				case USUARIO:
					return (object as Usuarios).nombre;
					
				case DISPOSITIVO:
					return (object as Dispositivos).codigo;
					
				default:
					return object.name;
			}
		}
		
		public function get typeLetter():String
		{
			switch(type)
			{
				case APLICACION:
					return "A";
					
				case PERFIL:
					return "P";
					
				case ENTIDAD:
					return "En";
					
				case TIPOREGISTRO:
					return "TR";
					
				case CAMPO:
					return "C";
					
				case FILTRO:
					return "F";
					
				case ESPECIFICO:
					return "Es";
					
				case USUARIO:
					return "U";
					
				case DISPOSITIVO:
					return "D";
					
				default:
					return "";
			}
		}
		
		public function get isPerfil():Boolean
		{
			return type == PERFIL;
		}
		
		public function toString():String
		{
			var ret:String = "";
			
			switch(type)
			{
				case PERFIL:
				case ENTIDAD:
				case TIPOREGISTRO:
					ret += "Hijos: "+children.length+"\n";
					break;
			}
			
			ret += object;
			
			return ret;
		}
		
		public function compara(tiTarget:TreeItem, forzado:Boolean = false):void
		{
			
			switch(type)
			{
				case APLICACION:
					break;
				
				case PERFIL:
					for each (var tiS:TreeItem in children) 
					{
						for each (var tiT:TreeItem in tiTarget.children) 
						{
							if (tiS.label == tiT.label)
							{
								tiS.compara(tiT);
								break;
							}
						}
					}
					
					break;
				
				case ENTIDAD:
					typeColor = object.compara(tiTarget.object);
					
					for each (var tiS:TreeItem in children) 
					{
						for each (var tiT:TreeItem in tiTarget.children) 
						{
							if (tiS.label == tiT.label)
							{
								tiS.compara(tiT);
								break;
							}
						}
					}
					
					break;
				
				case TIPOREGISTRO:
					typeColor = object.compara(tiTarget.object);
					
					for each (var tiS:TreeItem in children) 
					{
						for each (var tiT:TreeItem in tiTarget.children) 
						{
							if (tiS.label == tiT.label)
							{
								tiS.compara(tiT);
								break;
							}
						}
					}
					
					break;
				
				case FILTRO:
					typeColor = object.compara(tiTarget.object);
					break;
				
				case ESPECIFICO:
					typeColor = object.compara(tiTarget.object);
					break;
				
				case CAMPO:
					typeColor = object.compara(tiTarget.object);
					break;
				
				case USUARIO:
					typeColor = object.compara(tiTarget.object);
					break;
				
				case DISPOSITIVO:
//					typeColor = object.compara(tiTarget.object);
					break;
			}
		}
	}
}