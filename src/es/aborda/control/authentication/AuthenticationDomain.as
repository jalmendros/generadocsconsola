package es.aborda.control.authentication
{
	import com.salesforce.results.UserInfo;
	
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	[Bindable]
	public class AuthenticationDomain extends EventDispatcher
	{
		public var authorities:ArrayCollection;
		public var userInfo:UserInfo;
		public var usuario:String;
		
		public function get hasRole():Boolean
		{
			if (!authorities || authorities.length == 0)
				return false;
			
			for each (var obj:Object in authorities)
			{
				if ((obj as String) != null || (obj as String) != "" )					
					return true;
			}
			
			return false;
		}
		
		public function get isMaster():Boolean
		{
			if (!authorities || authorities.length == 0)
				return false;
			
			for each (var obj:Object in authorities)
			{
				if ((obj as String) == "ROL_MASTER")
					return true;
			}
			
			return false;
		}

		public function get isTrialOrLite():Boolean
		{
			if (!authorities || authorities.length == 0)
				return false;
			
			for each (var obj:Object in authorities)
			{
				if ((obj as String) == "ROL_TRIAL" || (obj as String) == "ROL_LITE")
					return true;
			}
			
			return false;
		}
						
		public function get isTrial():Boolean
		{
			if (!authorities || authorities.length == 0)
				return false;
			
			for each (var obj:Object in authorities)
			{
				if ((obj as String) == "ROL_TRIAL")
					return true;
			}
			
			return false;
		}
		
		public function get isLite():Boolean
		{
			if (!authorities || authorities.length == 0)
				return false;
			
			for each (var obj:Object in authorities)
			{
				if ((obj as String) == "ROL_LITE")
					return true;
			}
			
			return false;
		}
		
		public function get isAdmin():Boolean
		{
			if (!authorities || authorities.length == 0)
				return false;
			
			for each (var obj:Object in authorities)
			{
				if ((obj as String) == "ROL_ADMIN")
					return true;
			}
			
			return false;
		}
		
	}
}