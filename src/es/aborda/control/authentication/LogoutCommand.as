package es.aborda.control.authentication
{
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	import mx.core.FlexGlobals;
	import mx.managers.CursorManager;
	import mx.messaging.ChannelSet;
	import mx.rpc.AsyncToken;
	import mx.rpc.events.FaultEvent;
	
	public class LogoutCommand
	{
		[Inject(id="channelSet")]
		public var service:ChannelSet;
		
		public function execute(event:LogoutMessage):AsyncToken
		{
			CursorManager.setBusyCursor();
			return service.logout();
		}
		
		public function result(data:Object):void
		{
			CursorManager.removeBusyCursor();
			var urlRequest:URLRequest = new URLRequest(FlexGlobals.topLevelApplication.url);
			navigateToURL(urlRequest, "_self");
		}
		
		public function error(info:FaultEvent):void
		{
			CursorManager.removeBusyCursor();
		}
	}
}