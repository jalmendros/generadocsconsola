package es.aborda.control.authentication
{
	public class LoginMessage {
		private var _userName:String;
		private var _password:String;
		private var _callback:Function;

		public function get userName():String {
			return _userName;
		}

		public function get password():String {
			return _password;
		}

		public function get callback():Function {
			return _callback;
		}

		public function LoginMessage(userName:String, password:String, callback:Function = null) {
			_userName = userName;
			_password = password;
			_callback = callback;
		}
	}
}