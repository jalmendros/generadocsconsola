package es.aborda.control.authentication
{
	import mx.collections.ArrayCollection;
	import mx.managers.CursorManager;
	import mx.messaging.ChannelSet;
	import mx.rpc.AsyncToken;
	import mx.rpc.events.FaultEvent;
	import mx.utils.ObjectUtil;
	
	public class LoginCommand
	{
		[Inject(id="channelSet")]
		public var service:ChannelSet;
		
		[Inject]
		public var domain:AuthenticationDomain;
		
		private var callback:Function;
		
		public function execute(event:LoginMessage):AsyncToken
		{
			callback = event.callback;
			CursorManager.setBusyCursor();
			return service.login(event.userName, event.password);
		}
		
		public function result(data:Object):void
		{
			var authorities:Array = data.authorities as Array;
			
			domain.authorities = new ArrayCollection(authorities);
			domain.usuario = data.name;
			
			CursorManager.removeBusyCursor();
			
			if (ObjectUtil.compare(callback, null) != 0)
			{
				callback();
			}
		}
		
		public function error(info:FaultEvent):void
		{
			CursorManager.removeBusyCursor();
		}
	}
}