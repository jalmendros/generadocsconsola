package es.aborda.control
{
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	[Bindable]
	public class ComparaConsolaDomain extends EventDispatcher
	{
		public var logged:Boolean = false;
		public var informe:String = "";
		public var arrElementos:ArrayCollection = new ArrayCollection();
		public var arrPropiedades:ArrayCollection = new ArrayCollection();
		
	}
}