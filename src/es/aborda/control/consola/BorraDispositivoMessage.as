package es.aborda.control.consola
{
	public class BorraDispositivoMessage {
		public var dispositivoId:int;
		public var callback:Function;

		public function BorraDispositivoMessage(dispositivoId:int, callback:Function = null) {
			this.dispositivoId = dispositivoId;
			this.callback = callback;
		}
	}
}