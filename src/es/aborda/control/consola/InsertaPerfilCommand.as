package es.aborda.control.consola
{
	import mx.core.FlexGlobals;
	import mx.managers.CursorManager;
	import mx.rpc.AsyncToken;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.remoting.RemoteObject;
	import mx.utils.ObjectUtil;
	
	public class InsertaPerfilCommand
	{
		[Inject(id="controlConsola")]
		public var service:RemoteObject;
		
		[Inject]
		public var domain:ConsolaDomain;
		
		private var callback:Function;
		
		public function execute(event:InsertaPerfilMessage):AsyncToken
		{
			callback = event.callback;
			CursorManager.setBusyCursor();
			FlexGlobals.topLevelApplication.enabled = false;
			return service.insertaPerfil(event.perfil);
		}
		
		public function result(data:Object):void
		{
			CursorManager.removeBusyCursor();
			FlexGlobals.topLevelApplication.enabled = true;
			
			if (ObjectUtil.compare(callback, null) != 0)
			{
				domain.perfilId = data as int;
				callback(data as int);
			}
		}
		
		public function error(info:FaultEvent):void
		{
			CursorManager.removeBusyCursor();
			FlexGlobals.topLevelApplication.enabled = true;
		}
	}
}