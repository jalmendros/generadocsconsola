package es.aborda.control.consola
{
	import es.aborda.domain.Campos;

	public class InsertaCampoMessage {
		public var campo:Campos;
		public var callback:Function;

		public function InsertaCampoMessage(campo:Campos, callback:Function = null) {
			this.campo = campo;
			this.callback = callback;
		}
	}
}