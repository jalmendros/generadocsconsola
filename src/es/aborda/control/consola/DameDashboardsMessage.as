package es.aborda.control.consola
{
	public class DameDashboardsMessage {
		public var callback:Function;
		public var id:int;

		public function DameDashboardsMessage(id:int, callback:Function = null) {
			this.callback = callback;
			this.id = id;
		}
	}
}