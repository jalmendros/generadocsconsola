package es.aborda.control.consola
{
	import es.aborda.domain.Organizaciones;

	public class GrabaOrganizacionMessage {
		public var organizaciones:Organizaciones;
		public var callback:Function;

		public function GrabaOrganizacionMessage(organizaciones:Organizaciones, callback:Function = null) {
			this.organizaciones = organizaciones;
			this.callback = callback;
		}
	}
}