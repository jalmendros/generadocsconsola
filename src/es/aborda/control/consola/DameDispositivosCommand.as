package es.aborda.control.consola
{
	import mx.collections.ArrayCollection;
	import mx.core.FlexGlobals;
	import mx.managers.CursorManager;
	import mx.rpc.AsyncToken;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.remoting.RemoteObject;
	import mx.utils.ObjectUtil;
	
	public class DameDispositivosCommand
	{
		[Inject(id="controlConsola")]
		public var service:RemoteObject;
		
//		[Inject]
//		public var domain:ConsolaDomain;
		
		private var callback:Function;
		
		public function execute(event:DameDispositivosMessage):AsyncToken
		{
			callback = event.callback;
			CursorManager.setBusyCursor();
			FlexGlobals.topLevelApplication.enabled = false;
			return service.dameDispositivos(event.usuarioId);
		}
		
		public function result(data:Object):ArrayCollection
		{
			CursorManager.removeBusyCursor();
			FlexGlobals.topLevelApplication.enabled = true;
			
//			domain.arrDispositivos = data as ArrayCollection;
			
			if (ObjectUtil.compare(callback, null) != 0)
			{
				callback(data as ArrayCollection);
			}
			
			return data as ArrayCollection;
		}
		
		public function error(info:FaultEvent):void
		{
			CursorManager.removeBusyCursor();
			FlexGlobals.topLevelApplication.enabled = true;
		}
	}
}