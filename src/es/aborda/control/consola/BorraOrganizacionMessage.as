package es.aborda.control.consola
{
	public class BorraOrganizacionMessage {
		public var organizacionId:int;
		public var callback:Function;

		public function BorraOrganizacionMessage(organizacionId:int, callback:Function = null) {
			this.organizacionId = organizacionId;
			this.callback = callback;
		}
	}
}