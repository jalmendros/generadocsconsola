package es.aborda.control.consola
{
	public class DameEtiquetasMessage {
		public var callback:Function;
		public var id:int;

		public function DameEtiquetasMessage(id:int, callback:Function = null) {
			this.callback = callback;
			this.id = id;
		}
	}
}