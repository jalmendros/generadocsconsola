package es.aborda.control.consola
{
	public class DameDispositivosMessage {
		public var callback:Function;
		public var usuarioId:int;

		public function DameDispositivosMessage(usuarioId:int, callback:Function = null) {
			this.callback = callback;
			this.usuarioId = usuarioId;
		}
	}
}