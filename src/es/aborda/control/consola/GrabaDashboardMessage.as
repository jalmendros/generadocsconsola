package es.aborda.control.consola
{
	import es.aborda.domain.Dashboards;

	public class GrabaDashboardMessage {
		public var dashboard:Dashboards;
		public var callback:Function;

		public function GrabaDashboardMessage(dashboard:Dashboards, callback:Function = null) {
			this.dashboard = dashboard;
			this.callback = callback;
		}
	}
}