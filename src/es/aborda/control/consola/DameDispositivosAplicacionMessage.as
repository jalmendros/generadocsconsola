package es.aborda.control.consola
{
	public class DameDispositivosAplicacionMessage {
		public var callback:Function;

		public function DameDispositivosAplicacionMessage(callback:Function = null) {
			this.callback = callback;
		}
	}
}