package es.aborda.control.consola
{
	import es.aborda.domain.Organizaciones;

	public class InsertaOrganizacionMessage {
		public var organizaciones:Organizaciones;
		public var callback:Function;

		public function InsertaOrganizacionMessage(organizaciones:Organizaciones, callback:Function = null) {
			this.organizaciones = organizaciones;
			this.callback = callback;
		}
	}
}