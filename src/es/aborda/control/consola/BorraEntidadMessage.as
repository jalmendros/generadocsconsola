package es.aborda.control.consola
{
	public class BorraEntidadMessage {
		public var entidadId:int;
		public var callback:Function;

		public function BorraEntidadMessage(entidadId:int, callback:Function = null) {
			this.entidadId = entidadId;
			this.callback = callback;
		}
	}
}