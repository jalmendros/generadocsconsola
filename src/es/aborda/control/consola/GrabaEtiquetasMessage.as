package es.aborda.control.consola
{
	import mx.collections.ArrayCollection;

	public class GrabaEtiquetasMessage {
		public var perfilId:int;
		public var arrEtiquetas:ArrayCollection;
		public var callback:Function;

		public function GrabaEtiquetasMessage(perfilId:int, arrEtiquetas:ArrayCollection, callback:Function = null) {
			this.perfilId = perfilId;
			this.arrEtiquetas = arrEtiquetas;
			this.callback = callback;
		}
	}
}