package es.aborda.control.consola
{
	public class DuplicarPerfilMessage {
		public var perfilId:int;
		public var callback:Function;

		public function DuplicarPerfilMessage(perfilId:int, callback:Function = null) {
			this.perfilId = perfilId;
			this.callback = callback;
		}
	}
}