package es.aborda.control.consola
{
	import es.aborda.domain.Aplicaciones;

	public class GrabaAplicacionMessage {
		public var aplicacion:Aplicaciones;
		public var callback:Function;

		public function GrabaAplicacionMessage(aplicacion:Aplicaciones, callback:Function = null) {
			this.aplicacion = aplicacion;
			this.callback = callback;
		}
	}
}