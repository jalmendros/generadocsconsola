package es.aborda.control.consola
{
	import es.aborda.domain.TiposRegistro;

	public class InsertaTiposRegistroMessage {
		public var tiposRegistro:TiposRegistro;
		public var callback:Function;

		public function InsertaTiposRegistroMessage(tiposRegistro:TiposRegistro, callback:Function = null) {
			this.tiposRegistro = tiposRegistro;
			this.callback = callback;
		}
	}
}