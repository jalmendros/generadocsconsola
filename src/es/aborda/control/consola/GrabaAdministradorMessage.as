package es.aborda.control.consola
{
	import es.aborda.domain.Administradores;

	public class GrabaAdministradorMessage {
		public var administrador:Administradores;
		public var callback:Function;

		public function GrabaAdministradorMessage(administrador:Administradores, callback:Function = null) {
			this.administrador = administrador;
			this.callback = callback;
		}
	}
}