package es.aborda.control.consola
{
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	import es.aborda.domain.Entidades;
	import es.aborda.domain.TiposRegistro;
	
	[Bindable]
	public class ConsolaDomain extends EventDispatcher
	{
		private var _dias_para_caducar:int;
		
		public var url_api:String;
		
		private var _organizacionId:int;
		
		public var arrOrganizaciones:ArrayCollection;
		
		public var administradorId:int;
		
		public var arrAdministradores:ArrayCollection;
		
		private var _aplicacionId:int;
		
		public var licenciasMax:int;
		
		public var arrAplicaciones:ArrayCollection;
		
		private var _perfilId:int;
		
		public var arrPerfiles:ArrayCollection;
		
		private var _entidadId:int;
		
		public var entidad:Entidades;
		
		public var arrEntidades:ArrayCollection;
		
		public var arrFiltros:ArrayCollection;
		
		public var arrDashboards:ArrayCollection;
		
		public var arrLabels:ArrayCollection;
		
		public var arrTiposRegistro:ArrayCollection;
		
		private var _tiposRegistroId:int;
		
		public var tipoRegistro:TiposRegistro;
		
		public var arrCampos:ArrayCollection;
		
		public var campoId:int;
		
		private var _usuarioId:int;
		
		public var arrUsuarios:ArrayCollection;
		
		public var arrUsuariosApp:ArrayCollection;
		
		public var dispositivoId:int;
		
		public var arrDispositivos:ArrayCollection;
		
		public var especificoId:int;
		
		public var arrEspecificos:ArrayCollection;

		public var arrLogs:ArrayCollection;
		
		public var lite : Boolean = true; 
		
		public function get dias_para_caducar():int
		{
			return _dias_para_caducar;
		}
		
		public function set dias_para_caducar(value:int):void
		{
			_dias_para_caducar = value;
		}
		
		public function get usuarioId():int
		{
			return _usuarioId;
		}

		public function set usuarioId(value:int):void
		{
			_usuarioId = value;
			
			dispositivoId = 0;
			arrDispositivos = null;
		}

		public function get tiposRegistroId():int
		{
			return _tiposRegistroId;
		}

		public function set tiposRegistroId(value:int):void
		{
			_tiposRegistroId = value;
			
			campoId = 0;
			arrCampos = null;
		}

		public function get entidadId():int
		{
			return _entidadId;
		}

		public function set entidadId(value:int):void
		{
			_entidadId = value;
			
			arrFiltros = null;
			arrTiposRegistro = null;
			tiposRegistroId = 0;
			campoId = 0;
			arrCampos = null;
			tipoRegistro = null;
		}
		
		public function get perfilId():int
		{
			return _perfilId;
		}
		
		public function set perfilId(value:int):void
		{
			_perfilId = value;
			
			entidadId = 0;
			arrEntidades = null;
			arrFiltros = null;
			arrDashboards = null;
			arrTiposRegistro = null;
			tiposRegistroId = 0;
			campoId = 0;
			arrCampos = null;
			usuarioId = 0;
			arrUsuarios = null;
			dispositivoId = 0;
			arrDispositivos = null;
			especificoId = 0;
			arrEspecificos = null;
		}

		public function get aplicacionId():int
		{
			return _aplicacionId;
		}

		public function set aplicacionId(value:int):void
		{
			_aplicacionId = value;
			
			perfilId = 0;
			arrPerfiles = null;
			entidadId = 0;
			arrEntidades = null;
			arrFiltros = null;
			arrDashboards = null;
			arrTiposRegistro = null;
			tiposRegistroId = 0;
			campoId = 0;
			arrCampos = null;
			usuarioId = 0;
			arrUsuarios = null;
			dispositivoId = 0;
			arrDispositivos = null;
			especificoId = 0;
			arrEspecificos = null;
		}

		public function get organizacionId():int
		{
			return _organizacionId;
		}

		public function set organizacionId(value:int):void
		{
			_organizacionId = value;
			administradorId = 0;
			arrAdministradores = null;
			aplicacionId = 0;
			arrAplicaciones = null;
			arrUsuariosApp = null;
			perfilId = 0;
			arrPerfiles = null;
			entidadId = 0;
			arrEntidades = null;
			arrFiltros = null;
			arrDashboards = null;
			arrTiposRegistro = null;
			tiposRegistroId = 0;
			campoId = 0;
			arrCampos = null;
			usuarioId = 0;
			arrUsuarios = null;
			dispositivoId = 0;
			arrDispositivos = null;
			especificoId = 0;
			arrEspecificos = null;
		}
		

	}
}