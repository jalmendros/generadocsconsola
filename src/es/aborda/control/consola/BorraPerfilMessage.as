package es.aborda.control.consola
{
	public class BorraPerfilMessage {
		public var perfilId:int;
		public var callback:Function;

		public function BorraPerfilMessage(perfilId:int, callback:Function = null) {
			this.perfilId = perfilId;
			this.callback = callback;
		}
	}
}