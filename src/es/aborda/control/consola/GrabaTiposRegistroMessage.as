package es.aborda.control.consola
{
	import es.aborda.domain.TiposRegistro;

	public class GrabaTiposRegistroMessage {
		public var tiposRegistro:TiposRegistro;
		public var callback:Function;

		public function GrabaTiposRegistroMessage(tiposRegistro:TiposRegistro, callback:Function = null) {
			this.tiposRegistro = tiposRegistro;
			this.callback = callback;
		}
	}
}