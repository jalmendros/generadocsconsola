package es.aborda.control.consola
{
	public class CargaUsuariosMessage {
		public var callback:Function;

		public function CargaUsuariosMessage(callback:Function = null) {
			this.callback = callback;
		}
	}
}