package es.aborda.control.consola
{
	import es.aborda.domain.Perfiles;

	public class InsertaPerfilMessage {
		public var perfil:Perfiles;
		public var callback:Function;

		public function InsertaPerfilMessage(perfil:Perfiles, callback:Function = null) {
			this.perfil = perfil;
			this.callback = callback;
		}
	}
}