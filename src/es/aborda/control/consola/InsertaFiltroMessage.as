package es.aborda.control.consola
{
	import es.aborda.domain.Filtros;

	public class InsertaFiltroMessage {
		public var filtro:Filtros;
		public var callback:Function;

		public function InsertaFiltroMessage(filtro:Filtros, callback:Function = null) {
			this.filtro = filtro;
			this.callback = callback;
		}
	}
}