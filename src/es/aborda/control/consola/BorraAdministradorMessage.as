package es.aborda.control.consola
{
	public class BorraAdministradorMessage {
		public var administradorId:int;
		public var callback:Function;

		public function BorraAdministradorMessage(administradorId:int, callback:Function = null) {
			this.administradorId = administradorId;
			this.callback = callback;
		}
	}
}