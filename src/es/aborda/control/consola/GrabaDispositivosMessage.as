package es.aborda.control.consola
{
	import mx.collections.ArrayCollection;
	
	public class GrabaDispositivosMessage {
		public var arrDispositivos:ArrayCollection;
		public var callback:Function;

		public function GrabaDispositivosMessage(arrDispositivos:ArrayCollection, callback:Function = null) {
			this.arrDispositivos = arrDispositivos;
			this.callback = callback;
		}
	}
}