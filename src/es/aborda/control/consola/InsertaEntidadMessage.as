package es.aborda.control.consola
{
	import es.aborda.domain.Entidades;

	public class InsertaEntidadMessage {
		public var entidad:Entidades;
		public var callback:Function;

		public function InsertaEntidadMessage(entidad:Entidades, callback:Function = null) {
			this.entidad = entidad;
			this.callback = callback;
		}
	}
}