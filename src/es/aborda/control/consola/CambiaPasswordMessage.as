package es.aborda.control.consola
{
	public class CambiaPasswordMessage {
		public var userName:String;
		public var oldpass:String; 
		public var newpass:String;
		public var callback:Function;

		public function CambiaPasswordMessage(userName:String, oldpass:String, newpass:String, callback:Function = null) {
			this.userName = userName;
			this.oldpass = oldpass;
			this.newpass = newpass;
			this.callback = callback;
		}
	}
}