package es.aborda.control.consola
{
	import es.aborda.domain.Especificos;

	public class InsertaEspecificoMessage {
		public var especifico:Especificos;
		public var callback:Function;

		public function InsertaEspecificoMessage(especifico:Especificos, callback:Function = null) {
			this.especifico = especifico;
			this.callback = callback;
		}
	}
}