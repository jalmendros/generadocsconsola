package es.aborda.control.consola
{
	import es.aborda.domain.Usuarios;

	public class GrabaUsuarioMessage {
		public var usuario:Usuarios;
		public var callback:Function;

		public function GrabaUsuarioMessage(usuario:Usuarios, callback:Function = null) {
			this.usuario = usuario;
			this.callback = callback;
		}
	}
}