package es.aborda.control.consola
{
	import es.aborda.domain.Dispositivos;

	public class InsertaDispositivoMessage {
		public var dispositivo:Dispositivos;
		public var callback:Function;

		public function InsertaDispositivoMessage(dispositivo:Dispositivos, callback:Function = null) {
			this.dispositivo = dispositivo;
			this.callback = callback;
		}
	}
}