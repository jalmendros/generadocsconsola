package es.aborda.control.consola
{
	public class CargaCamposParaExportMessage {
		public var callback:Function;
		public var id_aplicaciones:int;
		
		public function CargaCamposParaExportMessage(id_aplicaciones:int, callback:Function = null) {
			this.callback = callback;
			this.id_aplicaciones = id_aplicaciones;
		}
	}
}