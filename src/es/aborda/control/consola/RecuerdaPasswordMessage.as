package es.aborda.control.consola
{
	public class RecuerdaPasswordMessage {
		public var userName:String;
		public var lang:String;
		public var callback:Function;

		public function RecuerdaPasswordMessage(userName:String, lang:String, callback:Function = null) {
			this.userName = userName;
			this.lang = lang;
			this.callback = callback;
		}
	}
}