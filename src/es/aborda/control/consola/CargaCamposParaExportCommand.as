package es.aborda.control.consola
{
	import mx.collections.ArrayCollection;
	import mx.core.FlexGlobals;
	import mx.managers.CursorManager;
	import mx.rpc.AsyncToken;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.remoting.RemoteObject;
	import mx.utils.ObjectUtil;
	
	public class CargaCamposParaExportCommand
	{
		[Inject(id="controlConsola")]
		public var service:RemoteObject;
		
		private var callback:Function;
		
		public function execute(event:CargaCamposParaExportMessage):AsyncToken
		{
			callback = event.callback;
			CursorManager.setBusyCursor();
			FlexGlobals.topLevelApplication.enabled = false;
			return service.cargaCamposParaExport(event.id_aplicaciones);
		}
		
		public function result(data:Object):void
		{
			CursorManager.removeBusyCursor();
			FlexGlobals.topLevelApplication.enabled = true;
			
			if (ObjectUtil.compare(callback, null) != 0)
			{
				callback(data as ArrayCollection);
			}
		}
		
		public function error(info:FaultEvent):void
		{
			CursorManager.removeBusyCursor();
			FlexGlobals.topLevelApplication.enabled = true;
		}
	}
}