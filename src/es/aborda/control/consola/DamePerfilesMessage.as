package es.aborda.control.consola
{
	public class DamePerfilesMessage {
		public var callback:Function;
		public var id:int;

		public function DamePerfilesMessage(id:int, callback:Function = null) {
			this.callback = callback;
			this.id = id;
		}
	}
}