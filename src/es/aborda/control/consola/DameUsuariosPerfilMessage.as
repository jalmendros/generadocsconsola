package es.aborda.control.consola
{
	public class DameUsuariosPerfilMessage {
		public var callback:Function;
		public var perfilId:int;

		public function DameUsuariosPerfilMessage(perfilId:int, callback:Function = null) {
			this.callback = callback;
			this.perfilId = perfilId;
		}
	}
}