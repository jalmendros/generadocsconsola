package es.aborda.control.consola
{
	public class DameAplicacionesMessage {
		public var callback:Function;
		public var organizacionId:int;

		public function DameAplicacionesMessage(organizacionId:int, callback:Function = null) {
			this.callback = callback;
			this.organizacionId = organizacionId;
		}
	}
}