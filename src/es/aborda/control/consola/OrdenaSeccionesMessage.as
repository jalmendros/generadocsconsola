package es.aborda.control.consola
{
	import mx.collections.ArrayCollection;

	public class OrdenaSeccionesMessage {
		public var obj:ArrayCollection;
		public var tipoRegistroId:int;
		public var callback:Function;

		public function OrdenaSeccionesMessage(obj:ArrayCollection, tipoRegistroId:int, callback:Function = null) {
			this.obj = obj;
			this.tipoRegistroId = tipoRegistroId;
			this.callback = callback;
		}
	}
}