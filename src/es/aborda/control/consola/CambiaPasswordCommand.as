package es.aborda.control.consola
{	
	import mx.controls.Alert;
	import mx.core.FlexGlobals;
	import mx.managers.CursorManager;
	import mx.resources.ResourceManager;
	import mx.rpc.AsyncToken;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.remoting.RemoteObject;
	import mx.utils.ObjectUtil;
	
	public class CambiaPasswordCommand
	{
		[Inject(id="controlConsola")]
		public var service:RemoteObject;
		
		[Inject]
		public var domain:ConsolaDomain;
		
		private var callback:Function;
							
		public function execute(event:CambiaPasswordMessage):AsyncToken
		{
			callback = event.callback;
			CursorManager.setBusyCursor();
			FlexGlobals.topLevelApplication.enabled = false;
			return service.cambiaPassword(event.userName, event.oldpass, event.newpass);
		}
		
		public function result(data:Object):void
		{
			CursorManager.removeBusyCursor();
			FlexGlobals.topLevelApplication.enabled = true;
						
			if (data.toString()=="false")
			{				
				Alert.show(ResourceManager.getInstance().getString('login', 'passIncorrecta'));	
			}
			else
			{				
				Alert.show(ResourceManager.getInstance().getString('login', 'passCambioOk'));
				if (ObjectUtil.compare(callback, null) != 0)
				{
					callback(data as Boolean);
				}
			}
			
			
		}
		
		public function error(info:FaultEvent):void
		{
			CursorManager.removeBusyCursor();
			FlexGlobals.topLevelApplication.enabled = true;
		}
	}
}