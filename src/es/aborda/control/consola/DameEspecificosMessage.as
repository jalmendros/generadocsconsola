package es.aborda.control.consola
{
	public class DameEspecificosMessage {
		public var callback:Function;
		public var id:int;

		public function DameEspecificosMessage(id:int, callback:Function = null) {
			this.callback = callback;
			this.id = id;
		}
	}
}