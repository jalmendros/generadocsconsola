package es.aborda.control.consola
{
	import es.aborda.domain.Especificos;

	public class GrabaEspecificoMessage {
		public var especifico:Especificos;
		public var callback:Function;

		public function GrabaEspecificoMessage(especifico:Especificos, callback:Function = null) {
			this.especifico = especifico;
			this.callback = callback;
		}
	}
}