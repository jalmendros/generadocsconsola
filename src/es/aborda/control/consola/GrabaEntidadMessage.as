package es.aborda.control.consola
{
	import es.aborda.domain.Entidades;

	public class GrabaEntidadMessage {
		public var entidad:Entidades;
		public var callback:Function;

		public function GrabaEntidadMessage(entidad:Entidades, callback:Function = null) {
			this.entidad = entidad;
			this.callback = callback;
		}
	}
}