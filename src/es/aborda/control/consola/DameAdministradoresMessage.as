package es.aborda.control.consola
{
	public class DameAdministradoresMessage {
		public var callback:Function;

		public function DameAdministradoresMessage(callback:Function = null) {
			this.callback = callback;
		}
	}
}