package es.aborda.control.consola
{
	public class DameFiltrosMessage {
		public var callback:Function;
		public var id:int;

		public function DameFiltrosMessage(id:int, callback:Function = null) {
			this.callback = callback;
			this.id = id;
		}
	}
}