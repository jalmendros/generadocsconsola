package es.aborda.control.consola
{
	public class DameLogMessage {
		public var callback:Function;
		public var desde:Date;
		public var hasta:Date;
		public var severidad:String;
		public var usuario:String;

		public function DameLogMessage(desde:Date, hasta:Date, severidad:String, usuario:String, callback:Function = null) {
			this.callback = callback;
			this.desde = desde;
			this.hasta = hasta;
			this.severidad = severidad;
			this.usuario = usuario;
		}
	}
}