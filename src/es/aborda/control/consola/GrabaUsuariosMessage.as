package es.aborda.control.consola
{
	import mx.collections.ArrayCollection;

	public class GrabaUsuariosMessage {
		public var arrUsuarios:ArrayCollection;
		public var callback:Function;

		public function GrabaUsuariosMessage(arrUsuarios:ArrayCollection, callback:Function = null) {
			this.arrUsuarios = arrUsuarios;
			this.callback = callback;
		}
	}
}