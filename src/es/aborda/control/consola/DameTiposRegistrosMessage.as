package es.aborda.control.consola
{
	public class DameTiposRegistrosMessage {
		public var callback:Function;
		public var id:int;

		public function DameTiposRegistrosMessage(id:int, callback:Function = null) {
			this.callback = callback;
			this.id = id;
		}
	}
}