package es.aborda.control.consola
{
	public class BorraAplicacionMessage {
		public var aplicacionId:int;
		public var callback:Function;

		public function BorraAplicacionMessage(aplicacionId:int, callback:Function = null) {
			this.aplicacionId = aplicacionId;
			this.callback = callback;
		}
	}
}