package es.aborda.control.consola
{
	public class DameEntidadesMessage {
		public var callback:Function;
		public var id:int;

		public function DameEntidadesMessage(id:int, callback:Function = null) {
			this.callback = callback;
			this.id = id;
		}
	}
}