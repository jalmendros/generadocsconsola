package es.aborda.control.consola
{
	import es.aborda.domain.Campos;

	public class GrabaCampoMessage {
		public var campo:Campos;
		public var callback:Function;

		public function GrabaCampoMessage(campo:Campos, callback:Function = null) {
			this.campo = campo;
			this.callback = callback;
		}
	}
}