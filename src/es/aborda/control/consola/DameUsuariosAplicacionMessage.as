package es.aborda.control.consola
{
	public class DameUsuariosAplicacionMessage {
		public var callback:Function;

		public function DameUsuariosAplicacionMessage(callback:Function = null) {
			this.callback = callback;
		}
	}
}