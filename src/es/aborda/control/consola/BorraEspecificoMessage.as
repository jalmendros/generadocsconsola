package es.aborda.control.consola
{
	public class BorraEspecificoMessage {
		public var especificoId:int;
		public var callback:Function;

		public function BorraEspecificoMessage(especificoId:int, callback:Function = null) {
			this.especificoId = especificoId;
			this.callback = callback;
		}
	}
}