package es.aborda.control.consola
{
	public class BorraUsuarioMessage {
		public var usuarioId:int;
		public var callback:Function;

		public function BorraUsuarioMessage(usuarioId:int, callback:Function = null) {
			this.usuarioId = usuarioId;
			this.callback = callback;
		}
	}
}