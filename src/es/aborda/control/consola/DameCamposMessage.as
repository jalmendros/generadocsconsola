package es.aborda.control.consola
{
	public class DameCamposMessage {
		public var callback:Function;
		public var id:int;
		
		public function DameCamposMessage(id:int, callback:Function = null) {
			this.callback = callback;
			this.id = id;
		}
	}
}