package es.aborda.control.consola
{
	import es.aborda.domain.Usuarios;

	public class InsertaUsuarioMessage {
		public var usuario:Usuarios;
		public var callback:Function;

		public function InsertaUsuarioMessage(usuario:Usuarios, callback:Function = null) {
			this.usuario = usuario;
			this.callback = callback;
		}
	}
}