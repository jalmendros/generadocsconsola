package es.aborda.control.consola
{
	public class BorraTiposRegistroMessage {
		public var tipoRegistroId:int;
		public var callback:Function;

		public function BorraTiposRegistroMessage(tipoRegistroId:int, callback:Function = null) {
			this.tipoRegistroId = tipoRegistroId;
			this.callback = callback;
		}
	}
}