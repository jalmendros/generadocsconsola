package es.aborda.control.consola
{
	public class BorraCampoMessage {
		public var campoId:int;
		public var callback:Function;

		public function BorraCampoMessage(campoId:int, callback:Function = null) {
			this.campoId = campoId;
			this.callback = callback;
		}
	}
}