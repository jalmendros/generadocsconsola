package es.aborda.control.consola
{
	public class BorraFiltroMessage {
		public var filtroId:int;
		public var callback:Function;

		public function BorraFiltroMessage(filtroId:int, callback:Function = null) {
			this.filtroId = filtroId;
			this.callback = callback;
		}
	}
}