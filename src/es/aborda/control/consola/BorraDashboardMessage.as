package es.aborda.control.consola
{
	public class BorraDashboardMessage {
		public var dashboardId:int;
		public var callback:Function;

		public function BorraDashboardMessage(dashboardId:int, callback:Function = null) {
			this.dashboardId = dashboardId;
			this.callback = callback;
		}
	}
}