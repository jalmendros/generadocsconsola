package es.aborda.control.consola
{
	import es.aborda.domain.Dispositivos;
	
	public class GrabaDispositivoMessage {
		public var dispositivo:Dispositivos;
		public var callback:Function;

		public function GrabaDispositivoMessage(dispositivo:Dispositivos, callback:Function = null) {
			this.dispositivo = dispositivo;
			this.callback = callback;
		}
	}
}