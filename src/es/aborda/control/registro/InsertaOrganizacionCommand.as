package es.aborda.control.registro
{
	import mx.core.FlexGlobals;
	import mx.managers.CursorManager;
	import mx.rpc.AsyncToken;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.remoting.RemoteObject;
	import mx.utils.ObjectUtil;
	
	import es.aborda.control.consola.ConsolaDomain;
	
	public class InsertaOrganizacionCommand
	{
		[Inject(id="controlRegistro")]
		public var service:RemoteObject;
		
		[Inject]
		public var domain:ConsolaDomain;
		
		private var callback:Function;
		
		public function execute(event:InsertaOrganizacionMessage):AsyncToken
		{
			callback = event.callback;
			CursorManager.setBusyCursor();
			FlexGlobals.topLevelApplication.enabled = false;
			return service.insertaOrganizacion(event.organizaciones);
		}
		
		public function result(data:Object):void
		{
			CursorManager.removeBusyCursor();
			FlexGlobals.topLevelApplication.enabled = true;
			
			if (ObjectUtil.compare(callback, null) != 0)
			{
				domain.organizacionId = data as int;
				callback(data as int);
			}
		}
		
		public function error(info:FaultEvent):void
		{
			CursorManager.removeBusyCursor();
			FlexGlobals.topLevelApplication.enabled = true;
		}
	}
}