package es.aborda.control.registro
{
	import es.aborda.domain.Aplicaciones;

	public class InsertaAplicacionMessage {
		public var aplicacion:Aplicaciones;
		public var callback:Function;

		public function InsertaAplicacionMessage(aplicacion:Aplicaciones, callback:Function = null) {
			this.aplicacion = aplicacion;
			this.callback = callback;
		}
	}
}