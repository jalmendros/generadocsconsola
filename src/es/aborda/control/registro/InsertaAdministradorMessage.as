package es.aborda.control.registro
{
	import es.aborda.domain.Administradores;

	public class InsertaAdministradorMessage {
		public var administrador:Administradores;
		public var callback:Function;

		public function InsertaAdministradorMessage(administrador:Administradores, callback:Function = null) {
			this.administrador = administrador;
			this.callback = callback;
		}
	}
}