package es.aborda.domain
{
	[Bindable]
	[RemoteClass(alias="es.aborda.dominio.Dashboards")]
	public class Dashboards
	{
		public function Dashboards()
		{
		}
		
		public var id:int;
		public var url:String;
		public var titulo:String;
		public var perfiles:Perfiles;
		
		public function clone():Dashboards
		{
			var dashboards:Dashboards = new Dashboards();
			
			dashboards.id = id;
			dashboards.url = url;
			dashboards.titulo = titulo;
			dashboards.perfiles = perfiles.clone();
			
			return dashboards;
		}
		
		public function toExport():Dashboards
		{
			var dashboards:Dashboards = new Dashboards();
			
			dashboards.id = id;
			dashboards.url = url;
			dashboards.titulo = titulo;
			
			return dashboards;
		}
	}
}