package es.aborda.domain
{
	import mx.collections.ArrayCollection;
	import mx.utils.ObjectUtil;

	[Bindable]
	[RemoteClass(alias="es.aborda.dominio.TiposRegistro")]
	public dynamic class TiposRegistro
	{
		public function TiposRegistro()
		{
		}
		
		public static var excludes:ArrayCollection = new ArrayCollection();
		public static function classInfo():Object
		{
			var excludesAux:ArrayCollection = new ArrayCollection(["id", "entidades", "error"]);
			excludesAux.addAll(excludes);
			var classInfo:Object = ObjectUtil.getClassInfo(new TiposRegistro(), excludesAux.toArray(), {includeReadOnly:false, uris:null, includeTransient:true});
			
			return classInfo;
		}
		
		public var id:int;
		public var recordtypeid:String;
		public var name:String;
		public var color:String;
		public var imagen:String;
		public var entidades:Entidades;
		public var tab_grouped:int;
		public var error:String;
		
		public function compara(target:TiposRegistro):uint
		{
			var color:uint = 0x88ff88;
			var properties:Array = TiposRegistro.classInfo().properties;
			error = "";
			
			for (var j:int=0; j < properties.length; j++)
			{
				var key:String=properties[j].localName;
				if (this[key] != target[key])
				{
					if (this[key] == null && target[key] == "" || this[key] == "" && target[key] == null)
						continue;
					
					color = 0xff8888;
					error += key + " = " + target[key] + "\n";
				}
			}
			
			return color;
		}
		
		public function toString():String
		{
			var ret:String = "recordtypeid = "+recordtypeid+"\n";
			ret += "name = "+name+"\n";
			ret += "color = "+color+"\n";
			ret += "imagen = "+imagen+"\n";
			ret += "tab_grouped = "+tab_grouped+"\n";
			if (error && error.length > 0)
				ret += "\nDIFERENCIAS:\n"+error;
			return ret;
		}
		
	}
}