package es.aborda.domain
{
	import mx.collections.ArrayCollection;
	import mx.utils.ObjectUtil;

	[Bindable]
	[RemoteClass(alias="es.aborda.dominio.Entidades")]
	public class Entidades
	{
		public function Entidades()
		{
		}
		
		public static var excludes:ArrayCollection = new ArrayCollection();
		public static function classInfo():Object
		{
			var excludesAux:ArrayCollection = new ArrayCollection(["id", "perfiles", "datagriddinamico", "error"]);
			excludesAux.addAll(excludes);
			var classInfo:Object = ObjectUtil.getClassInfo(new Entidades(), excludesAux.toArray(), {includeReadOnly:false, uris:null, includeTransient:true});
			
			return classInfo;
		}
		
		public var id:int;
		public var nombre:String;
		public var etiqueta:String;
		public var visible:Boolean;
		public var orden:int;
		public var filtros:Boolean;
		public var createable:Boolean;
		public var updateable:Boolean;
		public var bloqueado:Boolean;
		public var vista:String;
		public var datagriddinamico:Boolean;
		public var perfiles:Perfiles;
		public var error:String;
		
		public function compara(target:Entidades):uint
		{
			var color:uint = 0x88ff88;
			var properties:Array = Entidades.classInfo().properties;
			error = "";
			
			for (var j:int=0; j < properties.length; j++)
			{
				var key:String=properties[j].localName;
				if (this[key] != target[key])
				{
					if (this[key] == null && target[key] == "" || this[key] == "" && target[key] == null)
						continue;
					
					color = 0xff8888;
					error += key + " = " + target[key] + "\n";
				}
			}
			
			return color;
		}
		
		public function toString():String
		{
			var ret:String = "etiqueta = "+etiqueta+"\n";
			ret += "visible = "+visible+"\n";
			ret += "orden = "+orden+"\n";
			ret += "filtros = "+filtros+"\n";
			ret += "createable = "+createable+"\n";
			ret += "updateable = "+updateable+"\n";
			ret += "vista = "+vista+"\n";
			if (error && error.length > 0)
				ret += "\nDIFERENCIAS:\n"+error;
			return ret;
		}
	}
}