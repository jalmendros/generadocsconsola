package es.aborda.domain
{
	[Bindable]
	[RemoteClass(alias="es.aborda.dominio.clientes")]
	public class clientes
	{
		public function clientes()
		{
		}
		
		public var id:int;
		public var CompanyName:String;
		public var BillingPostalCode:String;
		public var BillingState:String;
		public var BillingStreet:String;
		public var BillingCity:String;
		public var BillingCountry:String;
		public var CompanyWebsite:String;
		public var VATid:String;
		public var ContactPersonName:String;
		public var ContactPersonLastName:String;
		public var ContactPersonPhone:String;
		public var ContactPersonEmail:String;
		public var Terms:Boolean;
		public var NewsLetter:Boolean;
		public var VATpercent:int;
		public var PriceBookId:String;
		public var IdSFAccount:String;
	}
}