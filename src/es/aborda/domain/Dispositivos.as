package es.aborda.domain
{
	import mx.collections.ArrayCollection;
	import mx.utils.ObjectUtil;

	[Bindable]
	[RemoteClass(alias="es.aborda.dominio.Dispositivos")]
	public class Dispositivos
	{
		public function Dispositivos()
		{
		}
		
		public static var excludes:ArrayCollection = new ArrayCollection();
		public static function classInfo():Object
		{
			var excludesAux:ArrayCollection = new ArrayCollection(["id", "usuarios", "instalado", "reinstalar", "error"]);
			excludesAux.addAll(excludes);
			var classInfo:Object = ObjectUtil.getClassInfo(new Dispositivos(), excludesAux.toArray(), {includeReadOnly:false, uris:null, includeTransient:true});
			
			return classInfo;
		}
		
		public var id:int;
		public var nombre:String;
		public var codigo:String;
		public var activo:Boolean;
		public var instalado:Boolean;
		public var reinstalar:Boolean;
		public var usuarios:Usuarios;
		public var debug:Boolean = false;
		public var logger:Boolean = false;
		public var logtoserver:Boolean = false;
		public var error:String;
		
		public function compara(target:Dispositivos):uint
		{
			var color:uint = 0xbbffbb;
			var properties:Array = Dispositivos.classInfo().properties;
			error = "";
			
			for (var j:int=0; j < properties.length; j++)
			{
				var key:String=properties[j].localName;
				if (this[key] != target[key])
				{
					if (this[key] == null && target[key] == "" || this[key] == "" && target[key] == null)
						continue;
					
					color = 0xffbbbb;
					error += key + " = " + target[key] + "\n";
				}
			}
			
			return color;
		}
		
		public function toString():String
		{
			var ret:String = "nombre = "+nombre+"\n";
			ret += "codigo = "+codigo+"\n";
			ret += "activo = "+activo+"\n";
			ret += "instalado = "+instalado+"\n";
			ret += "reinstalar = "+reinstalar+"\n";
			ret += "debug = "+debug+"\n";
			ret += "logger = "+logger+"\n";
			ret += "logtoserver = "+logtoserver+"\n";
			if (error && error.length > 0)
				ret += "\nDIFERENCIAS:\n"+error;
			return ret;
		}
	}
}