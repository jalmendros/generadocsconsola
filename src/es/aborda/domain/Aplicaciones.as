package es.aborda.domain
{
	[Bindable]
	[RemoteClass(alias="es.aborda.dominio.Aplicaciones")]
	public class Aplicaciones
	{
		public function Aplicaciones()
		{
		}
		
		public var id:int;
		public var aplicacion:String;
		public var email:String;
		public var nombre:String;
		public var telefono:String;
		public var url:String;
		public var licencias:int;
		public var maxdias:int = 29;
		public var organizaciones:Organizaciones;
		
		public function clone():Aplicaciones
		{
			var aplicaciones:Aplicaciones = new Aplicaciones();
			
			aplicaciones.id = id;
			aplicaciones.aplicacion = aplicacion;
			aplicaciones.email = email;
			aplicaciones.nombre = nombre;
			aplicaciones.telefono = telefono;
			aplicaciones.url = url;
			aplicaciones.licencias = licencias;
			aplicaciones.maxdias = maxdias;
			aplicaciones.organizaciones = organizaciones.clone();
			
			return aplicaciones;
		}
	}
}