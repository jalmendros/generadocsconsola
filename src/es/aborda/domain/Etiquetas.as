package es.aborda.domain
{
	[Bindable]
	[RemoteClass(alias="es.aborda.dominio.Etiquetas")]
	public class Etiquetas
	{
		public function Etiquetas()
		{
		}
		
//		public var id:int;
		public var etiqueta:String;
		public var idioma:String;
		public var traduccion:String;
		public var perfiles:Perfiles;
		
		public function clone():Etiquetas
		{
			var espec:Etiquetas = new Etiquetas();
			
//			espec.id = id;
			espec.etiqueta = etiqueta;
			espec.idioma = idioma;
			espec.traduccion = traduccion;
			espec.perfiles = perfiles.clone();
			
			return espec;
		}
		
		public function toExport():Etiquetas
		{
			var espec:Etiquetas = new Etiquetas();
			
//			espec.id = id;
			espec.etiqueta = etiqueta;
			espec.idioma = idioma;
			espec.traduccion = traduccion;
			
			return espec;
		}
	}
}