package es.aborda.domain
{
	[Bindable]
	[RemoteClass(alias="es.aborda.dominio.Administradores")]
	public class Administradores
	{
		public function Administradores()
		{
		}
		
		public var id:int;
		public var nombre:String;
		public var apellidos:String;
		public var email:String;
		public var telefono:String;
		public var pais:String;
		public var idioma:String;
		public var sf_username:String;
		public var sf_password:String;
		public var rol:String;
		public var organizaciones:Organizaciones;
		
		public function clone():Administradores
		{
			var administradores:Administradores = new Administradores();
			
			administradores.id = id;
			administradores.nombre = nombre;
			administradores.apellidos = apellidos;
			administradores.email = email;
			administradores.telefono = telefono;
			administradores.pais = pais;
			administradores.idioma = idioma;
			administradores.sf_username = sf_username;
			administradores.sf_password = sf_password;
			administradores.rol = rol;
			administradores.organizaciones = organizaciones.clone();
			
			return administradores;
		}
	}
}