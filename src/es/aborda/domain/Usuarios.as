package es.aborda.domain
{
	import mx.collections.ArrayCollection;
	import mx.utils.ObjectUtil;

	[Bindable]
	[RemoteClass(alias="es.aborda.dominio.Usuarios")]
	public class Usuarios
	{
		public function Usuarios()
		{
		}
		
		public static var excludes:ArrayCollection = new ArrayCollection();
		public static function classInfo():Object
		{
			var excludesAux:ArrayCollection = new ArrayCollection(["id", "perfiles", "fechasincro", "numsincro", "version", "error"]);
			excludesAux.addAll(excludes);
			var classInfo:Object = ObjectUtil.getClassInfo(new Usuarios(), excludesAux.toArray(), {includeReadOnly:false, uris:null, includeTransient:true});
			
			return classInfo;
		}
		
		public var id:int;
		public var nombre:String;
		public var activo:Boolean;
		public var formatofecha:String;
		public var fechasincro:Date;
		public var numsincro:int;
		public var version:String;
		public var export:Boolean;
		public var perfiles:Perfiles;
		public var error:String;
		
		public function compara(target:Usuarios):uint
		{
			var color:uint = 0xbbffbb;
			var properties:Array = Usuarios.classInfo().properties;
			error = "";
			
			for (var j:int=0; j < properties.length; j++)
			{
				var key:String=properties[j].localName;
				if (this[key] != target[key])
				{
					if (this[key] == null && target[key] == "" || this[key] == "" && target[key] == null)
						continue;
					
					color = 0xffbbbb;
					error += key + " = " + target[key] + "\n";
				}
			}
			
			return color;
		}
		
		public function toString():String
		{
			var ret:String = "nombre = "+nombre+"\n";
			ret += "activo = "+activo+"\n";
			ret += "formatofecha = "+formatofecha+"\n";
			ret += "export = "+export+"\n";
			if (error && error.length > 0)
				ret += "\nDIFERENCIAS:\n"+error;
			return ret;
		}
	}
}
