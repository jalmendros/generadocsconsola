package es.aborda.domain
{
	[Bindable]
	[RemoteClass(alias="es.aborda.dominio.Perfiles")]
	public class Perfiles
	{
		public function Perfiles()
		{
		}
		
		public var id:int;
		public var nombre:String;
		public var tamano_bbdd:int;
		public var sf_username:String;
		public var activo:Boolean;
		public var fechamodificacion:Date;
		public var aplicaciones:Aplicaciones;
		public var visitplanning:Boolean;
		public var gestion_pedidos:Boolean;
		public var auto_upgrade:Boolean;
		public var type:String;
		
		public function clone():Perfiles
		{
			var perfiles:Perfiles = new Perfiles();
			
			perfiles.id = id;
			perfiles.nombre = nombre;
			perfiles.tamano_bbdd = tamano_bbdd;
			perfiles.sf_username = sf_username;
			perfiles.activo = activo;
			perfiles.fechamodificacion = fechamodificacion;
			perfiles.aplicaciones = aplicaciones.clone();
			perfiles.visitplanning = visitplanning;
			perfiles.gestion_pedidos = gestion_pedidos;
			perfiles.auto_upgrade = auto_upgrade;
			perfiles.type = type;
			
			return perfiles;
		}
		
		public function toExport():Perfiles
		{
			var perfiles:Perfiles = new Perfiles();
			
			perfiles.id = id;
			perfiles.nombre = nombre;
			perfiles.tamano_bbdd = tamano_bbdd;
			perfiles.sf_username = sf_username;
			perfiles.activo = activo;
			perfiles.fechamodificacion = fechamodificacion;
			perfiles.visitplanning = visitplanning;
			perfiles.gestion_pedidos = gestion_pedidos;
			perfiles.auto_upgrade = auto_upgrade;
			perfiles.type = type;
			
			return perfiles;
		}
	}
}