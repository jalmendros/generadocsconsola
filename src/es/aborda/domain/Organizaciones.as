package es.aborda.domain
{
	[Bindable]
	[RemoteClass(alias="es.aborda.dominio.Organizaciones")]
	public class Organizaciones
	{
		public function Organizaciones()
		{
		}
		
		public var id:int;
		public var nombre:String;
		public var sf_id:String;
		public var gestion_territorio:Boolean;
		public var url_api:String;
		public var especifico:Boolean;
		public var timesincro:Number = 0;
		public var recarga_gt:Boolean;
		public var autolayout:Boolean;
		public var estilo:String;
		public var url_logo:String;
//		public var url_link_logo:String;
		public var date_created:Date;
		public var sf_domain:String;
		public var cliente:clientes;
		
		public function clone():Organizaciones
		{
			var organizaciones:Organizaciones = new Organizaciones(); 
			
			organizaciones.id = id;
			organizaciones.nombre = nombre;
			organizaciones.sf_id = sf_id;
			organizaciones.gestion_territorio = gestion_territorio;
			organizaciones.url_api = url_api;
			organizaciones.especifico = especifico;
			organizaciones.timesincro = timesincro;
			organizaciones.recarga_gt = recarga_gt;
			organizaciones.autolayout = autolayout;
			organizaciones.estilo = estilo;
			organizaciones.url_logo = url_logo;
//			organizaciones.url_link_logo = url_link_logo;
			organizaciones.date_created = date_created;
			organizaciones.sf_domain = sf_domain;
			organizaciones.cliente = cliente;
			
			return organizaciones;
		}
	}
}