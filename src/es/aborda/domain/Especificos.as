package es.aborda.domain
{
	import mx.collections.ArrayCollection;
	import mx.utils.ObjectUtil;

	[Bindable]
	[RemoteClass(alias="es.aborda.dominio.Especificos")]
	public class Especificos
	{
		public function Especificos()
		{
		}
		
		public static var excludes:ArrayCollection = new ArrayCollection();
		public static function classInfo():Object
		{
			var excludesAux:ArrayCollection = new ArrayCollection(["id", "perfiles", "descripcion", "error"]);
			excludesAux.addAll(excludes);
			var classInfo:Object = ObjectUtil.getClassInfo(new Especificos(), excludesAux.toArray(), {includeReadOnly:false, uris:null, includeTransient:true});
			
			return classInfo;
		}
		
		public var id:int;
		public var especifico:String;
		public var operador:String;
		public var entidad:String;
		public var parametros:String;
		public var etiqueta:String;
		public var modulo:String;
		public var descripcion:String;
		public var perfiles:Perfiles;
		public var error:String;
		
		public function compara(target:Especificos):uint
		{
			var color:uint = 0xbbffbb;
			var properties:Array = Especificos.classInfo().properties;
			error = "";
			
			for (var j:int=0; j < properties.length; j++)
			{
				var key:String=properties[j].localName;
				if (this[key] != target[key])
				{
					if (this[key] == null && target[key] == "" || this[key] == "" && target[key] == null)
						continue;
					
					color = 0xffbbbb;
					error += key + " = " + target[key] + "\n";
				}
			}
			
			return color;
		}
		
		public function toString():String
		{
			var ret:String = "especifico = "+especifico+"\n";
			ret += "operador = "+operador+"\n";
			ret += "entidad = "+entidad+"\n";
			ret += "parametros = "+parametros+"\n";
			ret += "etiqueta = "+etiqueta+"\n";
			ret += "modulo = "+modulo+"\n";
			if (error && error.length > 0)
				ret += "\nDIFERENCIAS:\n"+error;
			return ret;
		}
	}
}