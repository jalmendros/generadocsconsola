package es.aborda.domain
{
	import mx.collections.ArrayCollection;
	import mx.utils.ObjectUtil;
	
	[Bindable]
	[RemoteClass(alias="es.aborda.dominio.Campos")]
	public dynamic class Campos
	{
		public function Campos()
		{
		}
		
		public static var excludes:ArrayCollection = new ArrayCollection();
		public static function classInfo():Object
		{
			var excludesAux:ArrayCollection = new ArrayCollection(["id", "tiposRegistro", "error"]);
			excludesAux.addAll(excludes);
			var classInfo:Object = ObjectUtil.getClassInfo(new Campos(), excludesAux.toArray(), {includeReadOnly:false, uris:null, includeTransient:true});
			
			return classInfo;
		}
		
		public var id:int;
		public var name:String;
		public var label:String;
		public var required:Boolean;
		public var editable:Boolean;
		public var busqueda:Boolean;
		public var visible:Boolean;
		public var visible_grid:Boolean;
		public var seccion:String;
		public var orden:int;
		public var bloqueado:Boolean;
		public var posicion:String;
		public var orden2:int;
		public var orden_seccion:int;
		public var tiposRegistro:TiposRegistro;
		public var error:String;
		
		public function compara(target:Campos):uint
		{
			var color:uint = 0x88ff88;
			var properties:Array = Campos.classInfo().properties;
			error = "";
			
			for (var j:int=0; j < properties.length; j++)
			{
				var key:String=properties[j].localName;
				if (this[key] != target[key])
				{
					if (this[key] == null && target[key] == "" || this[key] == "" && target[key] == null)
						continue;
					
					color = 0xff8888;
					error += key + " = " + target[key] + "\n";
				}
			}
			
			return color;
		}
		
		public function toString():String
		{
			var ret:String = "label = "+label+"\n";
			ret += "required = "+required+"\n";
			ret += "editable = "+editable+"\n";
			ret += "busqueda = "+busqueda+"\n";
			ret += "visible = "+visible+"\n";
			ret += "visible_grid = "+visible_grid+"\n";
			ret += "seccion = "+seccion+"\n";
			ret += "orden = "+orden+"\n";
			ret += "posicion = "+posicion+"\n";
			ret += "orden2 = "+orden2+"\n";
			ret += "orden_seccion = "+orden_seccion+"\n";
			if (error && error.length > 0)
				ret += "\nDIFERENCIAS:\n"+error;
			return ret;
		}
	}
}