package es.aborda.domain
{
	import mx.collections.ArrayCollection;
	import mx.utils.ObjectUtil;

	[Bindable]
	[RemoteClass(alias="es.aborda.dominio.Filtros")]
	public class Filtros
	{
		public function Filtros()
		{
		}
		
		public static var excludes:ArrayCollection = new ArrayCollection();
		public static function classInfo():Object
		{
			var excludesAux:ArrayCollection = new ArrayCollection(["id", "entidades", "error"]);
			excludesAux.addAll(excludes);
			var classInfo:Object = ObjectUtil.getClassInfo(new Filtros(), excludesAux.toArray(), {includeReadOnly:false, uris:null, includeTransient:true});
			
			return classInfo;
		}
		
		public var id:int;
		public var filtro:String;
		public var activo:Boolean;
		public var entidades:Entidades;
		public var error:String;
		
		public function compara(target:Filtros):uint
		{
			var color:uint = 0x88ff88;
			var properties:Array = Filtros.classInfo().properties;
			error = "";
			
			for (var j:int=0; j < properties.length; j++)
			{
				var key:String=properties[j].localName;
				if (this[key] != target[key])
				{
					if (this[key] == null && target[key] == "" || this[key] == "" && target[key] == null)
						continue;
					
					color = 0xff8888;
					error += key + " = " + target[key] + "\n";
				}
			}
			
			return color;
		}
		
		public function toString():String
		{
			var ret:String = "filtro = "+filtro+"\n";
			ret += "activo = "+activo+"\n";
			if (error && error.length > 0)
				ret += "\nDIFERENCIAS:\n"+error;
			return ret;
		}
	}
}