package es.aborda.support
{
	import mx.collections.ArrayCollection;
	
	import es.aborda.components.TreeItem;

	public class Comparador
	{
		public function Comparador()
		{
		}
		
		public var arrElementos:ArrayCollection = new ArrayCollection();
		
		public function iniciaComparacion():void
		{
			for each (var tiSource:TreeItem in arrElementos)
			{
				for each (var tiTarget:TreeItem in arrElementos)
				{
					if (tiSource == tiTarget)
						continue;
					
					if (tiSource.typeLetter != tiTarget.typeLetter)
						continue;
					
					tiSource.compara(tiTarget, true);
				}
			}
		}
		
	}
}