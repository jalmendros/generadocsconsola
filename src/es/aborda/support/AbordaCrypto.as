package es.aborda.support
{
	import com.hurlant.crypto.Crypto;
	import com.hurlant.crypto.symmetric.ICipher;
	import com.hurlant.crypto.symmetric.IPad;
	import com.hurlant.crypto.symmetric.NullPad;
	import com.hurlant.util.Hex;
	
	import flash.utils.ByteArray;
	
	public class AbordaCrypto
	{
		public static function encrypt(txt:String):String
		{
			var kdata:ByteArray = Hex.toArray("077977ac9a5f27248bc05b369670ad15b77c7270420e709bbedb8e0d83d53731");
			
			return enc(txt, kdata);
		}
		
		public static function decrypt(txt:String):String
		{
			var kdata:ByteArray = Hex.toArray("74e7f59f834747da1180ed38f397bb125ec86cc3d1bcd1d9c7218f7f670a09b6");
			
			return dec(txt, kdata);
		}
		
		public static function encriptarCadena(txt:String):String
		{
			var kdata:ByteArray = Hex.toArray("bfa9a190ff61471d");
			
			return enc(txt, kdata);
		}
		
		public static function desencriptarCadena(txt:String):String
		{
			var kdata:ByteArray = Hex.toArray("bfa9a190ff61471d");
			
			return dec(txt, kdata);
		}
		
		private static function enc(txt:String, kdata:ByteArray):String
		{
			var data:ByteArray = Hex.toArray(Hex.fromString(txt));
			var pad:IPad = new NullPad;
			var mode:ICipher = Crypto.getCipher("simple-rc4-ctr", kdata, pad);
			
			pad.setBlockSize(mode.getBlockSize());
			mode.encrypt(data);
			var s:String = Hex.fromArray(data);
			//return Hex.toString(s);
			return s;
		}
		
		private static function dec(txt:String, kdata:ByteArray):String
		{
			var data:ByteArray = Hex.toArray(txt);
			var pad:IPad = new NullPad;
			var mode:ICipher = Crypto.getCipher("simple-rc4-ctr", kdata, pad);
			
			pad.setBlockSize(mode.getBlockSize());
			mode.decrypt(data);
			return Hex.toString(Hex.fromArray(data));
		}
	}
}