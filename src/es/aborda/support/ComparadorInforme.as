package es.aborda.support
{
	import flash.utils.Dictionary;
	
	import mx.collections.ArrayCollection;
	import mx.utils.ObjectUtil;
	
	import es.aborda.components.TreeItem;
	import es.aborda.domain.Entidades;

	public class ComparadorInforme
	{
		public function ComparadorInforme()
		{
		}
		
		private var tabLevel:int = 0;
		public var informe:String = "";
		public var arrElementos:ArrayCollection = new ArrayCollection();
		private var dicEntidadSource:Dictionary = new Dictionary();
		private var dicEntidadTarget:Dictionary = new Dictionary();
		
		public function iniciaComparacion():void
		{
			informe = "";
			addToInforme("Iniciando comparación:");
			addBR();
			addToInforme("COMPARANDO ENTIDADES:");
			
			for each (var tiSource:TreeItem in arrElementos)
			{
				addSection();
				for each (var tiTarget:TreeItem in arrElementos)
				{
					if (tiSource == tiTarget)
						continue;
					
					addToInforme("Comparando las entidades de '"+tiSource.orgName + "' con las entidades de '"+tiTarget.orgName + "':");
					addToInforme("'"+tiSource.orgName + "' tiene "+tiSource.children.length+" entidades y '"+tiTarget.orgName + "' tiene "+tiSource.children.length+" entidades.");
					tabLevel++;
					comparaEntidades(tiSource, tiTarget);
					addBR();
					addToInforme("COMPARANDO TIPOS DE REGISTRO:");
					addBR();
					comparaTiposRegistro(tiSource, tiTarget);
					tabLevel--;
				}
				
				addBR();
			}
			
			
		}
		
		private function comparaTiposRegistro(tiSource:TreeItem, tiTarget:TreeItem):void
		{
			if (!tiSource.children || tiSource.children.length == 0)
			{
				addToInforme("'"+tiSource.orgName + "' no tiene entidades.");
				return;
			}
			
			if (!tiTarget.children || tiTarget.children.length == 0)
			{
				addToInforme("'"+tiTarget.orgName + "' no tiene entidades.");
				return;
			}
			
			for each (var tiS:TreeItem in tiSource.children) 
			{
				for each (var tiT:TreeItem in tiTarget.children) 
				{
					if (tiS.label == tiT.label)
					{
						addToInforme("Tipos de registro de la entidad '"+tiS.label+"':");
						addToInforme("'"+tiSource.orgName + "' tiene "+tiS.children.length+" y '"+tiTarget.orgName + "' tiene "+tiT.children.length+".");
						
						break;
					}
				}
				
			}
			return;
			
			var properties:Array = getClassInfo(dicEntidadSource);
			var maxLength:int = 0;
			for (var j:int=0; j < properties.length; j++)
			{
				var key:String = properties[j];
				if (key.length > maxLength) maxLength = key.length +1;
				if (dicEntidadTarget.hasOwnProperty(key))
				{
				}
			}
			return;
			properties = getClassInfo(dicEntidadSource);
			for (j=0; j < properties.length; j++)
			{
				key = properties[j];
				if (key.length > maxLength) maxLength = key.length +1;
				if (!dicEntidadSource.hasOwnProperty(key))
					addToInforme("A '"+tiTarget.orgName + "' le sobra la entidad '"+key+"'.");
			}
			
			addBR();
			addToInforme("Comprobando ordenación");
			addToInforme("'"+tiSource.orgName+"'\t\t\t'"+tiTarget.orgName+"'");
			
			properties = getClassInfo(dicEntidadSource);
			for (j=0; j < properties.length; j++)
			{
				key = properties[j];
				if (dicEntidadTarget.hasOwnProperty(key))
				{
					if ((dicEntidadSource[key] as Entidades).orden != (dicEntidadTarget[key] as Entidades).orden)
					{
						addToInforme(setTab(key, maxLength)+"\t"+(dicEntidadSource[key] as Entidades).orden+"\t"+(dicEntidadTarget[key] as Entidades).orden+".");
					}
				}
			}
			
			addBR();
			addToInforme("Comprobando visibilidad");
			addToInforme("'"+tiSource.orgName+"'\t\t\t'"+tiTarget.orgName+"'");
			
			properties = getClassInfo(dicEntidadSource);
			for (j=0; j < properties.length; j++)
			{
				key = properties[j];
				if (dicEntidadTarget.hasOwnProperty(key))
				{
					if ((dicEntidadSource[key] as Entidades).visible != (dicEntidadTarget[key] as Entidades).visible)
					{
						addToInforme(setTab(key, maxLength)+"\t"+(dicEntidadSource[key] as Entidades).visible+"\t"+(dicEntidadTarget[key] as Entidades).visible+".");
					}
				}
			}
			
			addBR();
			addToInforme("Comprobando flag creación");
			addToInforme("'"+tiSource.orgName+"'\t\t\t'"+tiTarget.orgName+"'");
			
			properties = getClassInfo(dicEntidadSource);
			for (j=0; j < properties.length; j++)
			{
				key = properties[j];
				if (dicEntidadTarget.hasOwnProperty(key))
				{
					if ((dicEntidadSource[key] as Entidades).createable != (dicEntidadTarget[key] as Entidades).createable)
					{
						addToInforme(setTab(key, maxLength)+"\t"+(dicEntidadSource[key] as Entidades).createable+"\t"+(dicEntidadTarget[key] as Entidades).createable+".");
					}
				}
			}
			
			addBR();
			addToInforme("Comprobando flag edición");
			addToInforme("'"+tiSource.orgName+"'\t\t\t'"+tiTarget.orgName+"'");
			
			properties = getClassInfo(dicEntidadSource);
			for (j=0; j < properties.length; j++)
			{
				key = properties[j];
				if (dicEntidadTarget.hasOwnProperty(key))
				{
					if ((dicEntidadSource[key] as Entidades).updateable != (dicEntidadTarget[key] as Entidades).updateable)
					{
						addToInforme(setTab(key, maxLength)+"\t"+(dicEntidadSource[key] as Entidades).updateable+"\t"+(dicEntidadTarget[key] as Entidades).updateable+".");
					}
				}
			}
		}
		
		private function comparaEntidades(tiSource:TreeItem, tiTarget:TreeItem):void
		{
			if (!tiSource.children || tiSource.children.length == 0)
			{
				addToInforme("'"+tiSource.orgName + "' no tiene entidades.");
				return;
			}
			
			if (!tiTarget.children || tiTarget.children.length == 0)
			{
				addToInforme("'"+tiTarget.orgName + "' no tiene entidades.");
				return;
			}
			
			for each (var tiEntidadSource:TreeItem in tiSource.children)
			{
				dicEntidadSource[tiEntidadSource.label] = tiEntidadSource.object;
			}
			for each (var tiEntidadTarget:TreeItem in tiTarget.children)
			{
				dicEntidadTarget[tiEntidadTarget.label] = tiEntidadTarget.object;
			}
			
			var properties:Array = getClassInfo(dicEntidadSource);
			var maxLength:int = 0;
			for (var j:int=0; j < properties.length; j++)
			{
				var key:String = properties[j];
				if (key.length > maxLength) maxLength = key.length +1;
				if (!dicEntidadTarget.hasOwnProperty(key))
					addToInforme("A '"+tiTarget.orgName + "' le falta la entidad '"+key+"'.");
			}
			
			properties = getClassInfo(dicEntidadTarget);
			for (j=0; j < properties.length; j++)
			{
				key = properties[j];
				if (key.length > maxLength) maxLength = key.length +1;
				if (!dicEntidadSource.hasOwnProperty(key))
					addToInforme("A '"+tiTarget.orgName + "' le sobra la entidad '"+key+"'.");
			}
			
			addBR();
			addToInforme("Comprobando ordenación");
			tabLevel++;
			addToInforme("'"+tiSource.orgName+"'\t\t\t'"+tiTarget.orgName+"'");
			
			properties = getClassInfo(dicEntidadSource);
			for (j=0; j < properties.length; j++)
			{
				key = properties[j];
				if (dicEntidadTarget.hasOwnProperty(key))
				{
					if ((dicEntidadSource[key] as Entidades).orden != (dicEntidadTarget[key] as Entidades).orden)
					{
						addToInforme(setTab(key, maxLength)+"\t"+(dicEntidadSource[key] as Entidades).orden+"\t"+(dicEntidadTarget[key] as Entidades).orden+".");
					}
				}
			}
			tabLevel--;
			
			addBR();
			addToInforme("Comprobando visibilidad");
			tabLevel++;
			addToInforme("'"+tiSource.orgName+"'\t\t\t'"+tiTarget.orgName+"'");
			
			properties = getClassInfo(dicEntidadSource);
			for (j=0; j < properties.length; j++)
			{
				key = properties[j];
				if (dicEntidadTarget.hasOwnProperty(key))
				{
					if ((dicEntidadSource[key] as Entidades).visible != (dicEntidadTarget[key] as Entidades).visible)
					{
						addToInforme(setTab(key, maxLength)+"\t"+(dicEntidadSource[key] as Entidades).visible+"\t"+(dicEntidadTarget[key] as Entidades).visible+".");
					}
				}
			}
			tabLevel--;
			
			addBR();
			addToInforme("Comprobando flag creación");
			tabLevel++;
			addToInforme("'"+tiSource.orgName+"'\t\t\t'"+tiTarget.orgName+"'");
			
			properties = getClassInfo(dicEntidadSource);
			for (j=0; j < properties.length; j++)
			{
				key = properties[j];
				if (dicEntidadTarget.hasOwnProperty(key))
				{
					if ((dicEntidadSource[key] as Entidades).createable != (dicEntidadTarget[key] as Entidades).createable)
					{
						addToInforme(setTab(key, maxLength)+"\t"+(dicEntidadSource[key] as Entidades).createable+"\t"+(dicEntidadTarget[key] as Entidades).createable+".");
					}
				}
			}
			tabLevel--;
			
			addBR();
			addToInforme("Comprobando flag edición");
			tabLevel++;
			addToInforme("'"+tiSource.orgName+"'\t\t\t'"+tiTarget.orgName+"'");
			
			properties = getClassInfo(dicEntidadSource);
			for (j=0; j < properties.length; j++)
			{
				key = properties[j];
				if (dicEntidadTarget.hasOwnProperty(key))
				{
					if ((dicEntidadSource[key] as Entidades).updateable != (dicEntidadTarget[key] as Entidades).updateable)
					{
						addToInforme(setTab(key, maxLength)+"\t"+(dicEntidadSource[key] as Entidades).updateable+"\t"+(dicEntidadTarget[key] as Entidades).updateable+".");
					}
				}
			}
			tabLevel--;
			
			addBR();
		}
		
		private function setTab(txt:String, maxLength:int):String
		{
			return txt + ":" + fill(maxLength - txt.length);
		}
		
		private function getClassInfo(obj:Object):Array
		{
			try
			{
				var classInfo:Object = ObjectUtil.getClassInfo(obj, [], {includeReadOnly:false, uris:null, includeTransient:true});
				return classInfo.properties;
			} 
			catch(error:Error) {}
			
			return [];
		}
		
		private function fill(length:int, char:String = " "):String
		{
			var ret:String = "";
			for (var i:int = 0; i < length; i++) 
			{
				ret += char;
			}
			
			return ret;
		}
		
		private function addToInforme(txt:String):void
		{
			informe += fill(tabLevel, "\t") + txt;
			addBR();
		}
		
		/*
		private function appendToInforme(txt:String):void
		{
			informe += txt;
		}
		*/
		
		private function addSection():void
		{
			informe += fill(30, "*");
			addBR();
		}
		
		private function addBR():void
		{
			informe += "\n";
		}
		
	}
}